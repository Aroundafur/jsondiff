# README #
clone, run
virtualenv --no-site-packages --distribute .env && source .env/bin/activate && pip install -r requirements.txt

now you're all set.

In my project I've provided 4 endpoints:
urls = (

    '/', 'index', #for testing

    '/v1/diff', 'diff', #get diff of two other endpoints

    '/v1/diff/right/(.*)', 'right', #get right endpoint data

    '/v1/diff/left/(.*)', 'left' #get left endpoint data

)

I've created classes for every endpoint, for pushing and pulling the data from a global list, "db"
The data was then extracted from this list for further calculation. 

I've used difflib library for delta between two strings.

unittests can be run by setting WEBPY_ENV=test and running nosetests:
WEBPY_ENV=test nosetests

I worked with virtualenv
requirements.txt shows env properties.

the app is run by:

python code.py <port>
def port is 8080, but you can change it to other free port in your system.

then localhost:8080/v1/diff/(.*)