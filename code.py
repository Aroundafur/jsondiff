import web
import os
import json
import difflib



db = ["left", "right"]

urls = (
    '/', 'index', #for testing
    '/v1/diff', 'diff',  #diff result
    '/v1/diff/right/(.*)', 'right', #right entry
    '/v1/diff/left/(.*)', 'left'    #left entry
)
app = web.application(urls, globals())

class left:
      def GET(self, jsonstr):
	web.header('Content-Type', 'application/json')
        db.insert(0,json.dumps(jsonstr))
	return jsonstr

class right:
      def GET(self, jsonstr):
        web.header('Content-Type', 'application/json')
        db.insert(1,json.dumps(jsonstr))
        return jsonstr

class diff:
    def GET(self):
      web.header('Content-Type', 'application/json')
      diff = ('{} => {}'.format(db[0],db[1])) + "\n"
      if db[0] == db[1]:
	return diff + db[0] + " Equal to " +  db[1]
      else:  
	for i,s in enumerate(difflib.ndiff(db[0],db[1])):
	     if s[0]==' ': continue
             elif s[0]=='-':
                 return diff + u'Delete "{}" from position {}'.format(s[-1],i)
             elif s[0]=='+':
                 return diff + u'Add "{}" to position {}'.format(s[-1],i)   

class index:

    #just another way to set page header
    def __init__(self):
        return web.header('Content-Type', 'text/html')
    #for test
    def GET(self):
        return 'Welcome to jsondiff'
     
#if running tests set env var to 'test'.
def is_test():
    if 'WEBPY_ENV' in os.environ:
       return os.environ['WEBPY_ENV'] == 'test'

if (not is_test()) and __name__ == "__main__":
    app.run()  

